from PIL import Image
import sys
import os
import base64
size = 128, 128

### data is base64 encoded by scheduler
def thumbnail(data=None):
   ### function should take responsiblity of decoding and manuplating data
   ### At high level we will do data validation 
   ### but low level tasks are delegated to function
   data = base64.b64decode(data)
   path = '/tmp/tmp.jpeg'  
   outfile = os.path.splitext(path)[0] + ".thumbnail"
   with open(path,'wb') as fout:
       fout.write(data)
   im = Image.open(path)
   im.thumbnail(size)
   im.save(outfile, "JPEG")
   with open(outfile) as fout:
       ret = fout.read()

   ret = base64.b64encode(ret)   
   return ret


