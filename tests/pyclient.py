import json
import argparse
import requests

URL='http://192.168.0.5:5000/'
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

def send(function,jsonpayload):
         
        print jsonpayload
	posturl = "{}{}".format(URL,function)
        ret = requests.post(posturl,json=jsonpayload,headers=headers)
	print ret





if __name__=="__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("-function", help="function name")
	parser.add_argument("-jsonfile", help="json file containing function args")
	args = parser.parse_args()
        
	with open(args.jsonfile) as fin:
	     jsonpayload = json.loads(fin.read())
  	send(args.function,jsonpayload)
