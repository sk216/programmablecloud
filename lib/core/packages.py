import pip
import importlib


def buildpackages(*packages):
    for package in packages:
        try:
            importlib.import_module(package)
        except ImportError:
            pip.main(['install',package,'-q','--ignore-installed'])  
            #pip.main(['install',package,'--ignore-installed'])
            ### this is a hack ,the pillow install packages as  PIL  
            if package == 'Pillow': package = 'PIL'    
            globals()[package] = importlib.import_module(package)
            
    return        
