import redis


class task:
    '''
       the class manages redis queue
       we use redis queue to enqueue and process jobs. Since many of backend jobs has long IO 
       we can delegate this job to backgroud processes.
    '''
    @classmethod
    def create(cls,taskid=None,redis_connection=None,timeout=None,func_name=None,**func_args):
        task = cls(redis_connection)
        task.taskid     = taskid
        task.func_name  = func_name
        task.func_args  = func_args
        task.timeout    = timeout
        task.result     = None
        task.status     = None
        return task

    def __init__(self,redis_connection):
        '''
           redis job 
           fun_name=<>,args=<>,timout=<>
        '''
        self.redis_connection = redis_connection

    @property 
    def get_taskid(self):
        #return self.jobid 
        pass

    def todict(self):
        obj = {}
        obj['func_name']  = self.func_name
        obj['func_args']  = self.func_args
        obj['timeout']    = self.timeout
        obj['taskid']     = self.taskid
        obj['result']     = self.result
        obj['status']     = self.status
        return obj
    
    def setTask(self,taskid,status=None):
        jobObj = self.todict()
	jobObj['status'] = status
        self.redis_connection.hmset(taskid, jobObj)
        print "saving job to redis key "

    @staticmethod 
    def getTask(connection,taskid):
        return connection.hgetall(taskid)  

    @staticmethod 
    def updateTask(connection,jobObj):
        taskid = jobObj['taskid']
        print "updating ++",jobObj
        connection.hmset(taskid, jobObj)


#onnection =  redis.StrictRedis(host='localhost')

#job = task.create(taskid=4,func_name='piarea',func_args=2,redis_connection=connection)
#job.setJob()
#print job.getJob('test')


