
#from abc import ABC, abstractmethod
import abc
from ast import literal_eval
from parseconfig import ProcessConfig
import imp
import functools
from packages import buildpackages


class funfactory(object):
      """
        implements base factory function 
      """
      __metaclass__ = abc.ABCMeta
      def __init__(self):
          self._init()

      def _init():
           self.intialize_fun() 

      @abc.abstractmethod
      def initialize_fun(self):
           raise NotImplementedError("not implemented")


class loadfuns(funfactory):
      """load the function dynamically and make available 
         for lib
      """
      def __init__(self,cfg):
          self.cfg = cfg
          self.funcname = None
          self.func_cache  = {}
          self.func_desc   = {}
          self.func_args = {}
          self.do_init()

      def do_init(self):
          #  To do caching
          buildpackages(*self.cfg.packages) 
          for cfg in self.cfg.config:
              self.initialize_fun(cfg)

      def initialize_fun(self,cfg):
          #print "now i am intializing the funs"
          funcname  = cfg['function']['name']    
          funcentry = cfg['function']['entry']
          doc       = cfg['function']['description']
          args     = cfg['function']['input']
          fp, pathName, description = imp.find_module(funcname,['functions'])  #TO DO config standardization 
          self.funcname = funcname
          funcname = imp.load_module(funcentry,fp,pathName,description)
          self.func_cache[funcname.__name__]  = funcname
          self.func_args[funcname.__name__]   = args
          self.func_desc[funcname.__name__]   = doc

      @property 
      def funnames(self):
          return self.func_cache.keys()
      
      def fundesc(self,fun):
          return self.func_desc[fun]

      def func_input(self,fun):
          return self.func_args[fun]

      def execfunc(self,funcname,arg):
          print "Executing {}  with arg {}".format(funcname,arg)
          if isinstance(arg,str):
             arg = literal_eval(arg)
          A = self.func_cache[funcname]
          fun =  getattr(A,funcname)
          return fun(**arg)

def config_init():
    cfg = ProcessConfig('config')
    return cfg

def servfuns():
    cfg  = config_init()
    funs = loadfuns(cfg)
    return funs

#servfuns()









