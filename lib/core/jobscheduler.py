import tasks
import redis
from worker import worker,workers
import Queue

# this should come from config
WORKERS=4

class Job:

    @classmethod
    def createJobrequest(cls,funcname=None,**kargs):
        '''
            job constructor
        '''
        return cls(funcname,**kargs)

    def __init__(self,funcname,**kargs):
        self.funcname = funcname
        self.params    = kargs
        self.status   = None

class jobscheduler(workers):

    def __init__(self,connection=None):
        self.jobs  = Queue.Queue()
        self.taskid = 0 ## this is dummy value   
        self.connection = connection
        self.workers = []
        self._initialize()
    
    def gentaskid(self):
        self.taskid+=1  # replace this with uuid
        return self.taskid
    
    def _initialize(self):
        ## lazy start
        for i in range(WORKERS):
           self.worker= worker(connection=self.connection)
           self.worker.start()
           print "registering ", self.worker,self.worker.state
           self.workers.append(self.worker)

    def getavailableworker(self):
        # change this func later 
        while True:
            for worker in self.workers:
                if worker.state == 'AVAILABLE':
                   return worker
        
    def notifyworkers(self,taskid):
            ## TO DO check worker state
            worker = self.getavailableworker()
            print "I am notify the next available worked for task"
            workers.notifyworker(worker,taskid,self.connection)

    def enqueueJobs(self,func=None,**kargs):
        '''
            flask invokes this function 
          to eneque jobs to the backend 
        '''
        taskid = self.gentaskid()
        job = Job.createJobrequest(funcname=func,**kargs)
        job = tasks.task.create(taskid=taskid,redis_connection=self.connection,func_name=job.funcname,**job.params)
        job.setTask(taskid,status='PENDING')
        self.notifyworkers(taskid)
        return taskid

    def getJobstatus(self,taskid):
        task = tasks.task.getTask(self.connection,taskid)
        if task['status'] == 'DONE':
            if task['result'] is not None:
                return task['result']
        return False  

connection =  redis.StrictRedis(host='localhost')
def init():
    connection = redis.StrictRedis(host='localhost')
    job   =   jobscheduler(connection=connection)
    return job
