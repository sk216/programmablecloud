import time
from threading  import Thread
from funfactory import servfuns
import  tasks
from ast import literal_eval

class  workers:
    ''' 
       workers class
    '''
    def __init__(self):
        self.workers = []
    
    def register_workers(self,worker):
        '''
           register all the workers, check work state 
           and register them for JOB. 

        '''
    @staticmethod
    def notifyworker(worker,taskid,connection=None):
        ### read the redis queue and notify worker
        print "notifying",taskid
        jobobj =  tasks.task.getTask(connection,taskid)
        funcname = jobobj['func_name']
        funcargs = jobobj['func_args']
        print "executing the func  {} with params {}".format(funcname,funcargs)
        worker.execfunction(jobobj)
        print "notification done"

###########################################                              

class worker(Thread):
    def __init__(self,connection=None):
        Thread.__init__(self)
        self.state = 'AVAILABLE'
        self.connection = connection
        self.stop = True
        self.execfun = False
        self.params  = None
        self.funs = servfuns()
        self.job  = None
        self.DECODE = False
        

    def execfunction(self,job):
        if self.execfun:
            print "Already running func try later"
        self.job = job
        self.execfun = True
        self.state   = 'RUNNING'

    def stopworker(self):
        pass

    def decodefuncargs(self,fun):
        self.args =  self.funs.func_input(fun)
        for arg,paramKey in zip(self.args,self.params.keys()):
             print arg,self.params[paramKey]
             # To do input verification

    def kill(self):
        '''
            kill the thread, remmeber the job 
            may go into unknown state. makesure
            job status timesout or go into availble state.
        '''
        self.stop = False

    def cacheresult(self):
        '''
           cache result in queue since thread execute asynchronosly 
           store results in redis cache

        '''
        tasks.task.updateTask(self.connection,self.job)

    def run(self):
          while self.stop:
                if self.execfun:
        	   self.function = self.job['func_name']
        	   self.params   = self.job['func_args']
                   if isinstance(self.params,str):
                         self.params = literal_eval(self.params)
                   self.decodefuncargs(self.function)
                   ret =  self.funs.execfunc(self.function,self.params)
                   print "Executed {} with param {} ret is {}".format(self.function,self.params,ret)
                   self.job['status'] = 'DONE'
                   self.job['result'] =  ret 
                   self.cacheresult()
                   self.execfun = False 
                   self.state = 'AVAILABLE'  


