from cloud import cloud


class cloudService(cloud):

   def __init__(self,name,template=None,**kargs):
        '''
          add init variables here for cloud services
        '''
        self.region            = kargs['region']
        self.availabiltyzone   = kargs['availabiltyzone']
        self.template          = template

          
   @classmethod
   def makecloudservice(cls,name,**kargs):
       return cls(name,**kargs)

   def _createAWSInstance(self):
       '''
          Internal Method to create AWS instance   
       '''
       pass
   
   def createInstance(self):
       '''
          create Instance on requested Cloud Service 
          try to use a template to create an instance 
       '''
       #self.ec2.create_instances(ImageId='<ami-image-id>', MinCount=1, MaxCount=5)
       
   def deleteInstance(self):
       pass

   def modifyInstance(self):
       pass 
   
   def updateInstance(self):
       pass

   def servicesrequested(self):
       pass
    

def getCloudService(name,template=None,**kargs):
       svc =  cloudService.makecloudservice('name',**kargs)
       return svc
   


