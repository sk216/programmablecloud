from flask import Flask ,jsonify,request
from lib.core.jobscheduler import init
app  = Flask(__name__)
funs = init()

def createtmpfile(filename):
    path = os.path.join('/tmp',filename)
    with open(path,'wb') as fout:
         fout.write(filename)
    return path

# To do fix 
@app.route('/listfuns')
def getfuns():
    #return jsonify(funs.funnames)
    pass

#To do fix as change in invocation
@app.route('/desc/<funname>')
def fundesc(funname):
    #return jsonify(funs.fundesc(funname))
    pass

@app.route('/<path:path>',methods=['GET', 'POST'])
def default_handle(path):
    data = request.json
    args = {}
    for k,v in data.iteritems():
        args[k] = v 
    taskid = funs.enqueueJobs(path,**args)
    return jsonify(taskid)

@app.route('/jobstatus/<int:jobid>')
def getjobstatus(jobid=None):
    ret = funs.getJobstatus(jobid)
    if not ret:
       return jsonify("PENDING")
    return jsonify(ret)
        
if __name__=="__main__":
    app.run(host='0.0.0.0')
